# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models

class Participant(models.Model):
    """
    Base model for clinic clients
    """
    name = models.CharField(max_length='150')
    date = models.DateField(auto_now_add = False)
    
    class Meta:
        verbose_name = u'participant'
        verbose_name_plural = 'participants'
        ordering = ('-name',)